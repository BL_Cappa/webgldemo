﻿using UnityEngine;

public class DownloadProject : MonoBehaviour
{
    private void Awake()
    {
        UIInputs.Instance.DownloadProject.onClick.AddListener(OpenDownloadProjectPag);
    }

    private void OpenDownloadProjectPag()
    {
        Application.OpenURL("https://drive.google.com/drive/folders/1mqameM7pA7QMDDg3IjP9JJGPpfns0FdB?usp=sharing");
    }
}