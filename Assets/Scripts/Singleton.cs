﻿using UnityEngine;  

/// <summary>
/// Heredar de esta clase para crear un singleton especialmente preparado para Unity
/// ej: public class MyClassName: Singleton <MyClassName>{}
/// </summary>
public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    // Para comprobar si la instancia sera destruida
    private static bool m_ShuttingDown = false;
    private static object m_Lock = new object();
    private static T m_Instance;
    
    // Se accede a la instancia del singleton mediante esta propiedad
    public static T Instance
    {
        get
        {
            if (m_ShuttingDown)
            {
                Debug.LogWarning("[Singleton] Instance '" + typeof(T) +
                    "' already destroyed. Returning null.");
                return null;
            }

            lock (m_Lock)
            {
                if (m_Instance == null)
                {
                    // Verificar si existe la instancia
                    m_Instance = (T)FindObjectOfType(typeof(T));

                    // Si la instancia no existe, crear intancia nueva
                    if (m_Instance == null)
                    {
                        // Se crea un objeto y se a;ade la instancia
                        var singletonObject = new GameObject();
                        m_Instance = singletonObject.AddComponent<T>();
                        singletonObject.name = typeof(T).ToString() + " (Singleton)";

                        // Hacer persistente la instancia
                        DontDestroyOnLoad(singletonObject);
                    }
                }

                return m_Instance;
            }
        }
    }

    private void OnApplicationQuit()
    {
        m_ShuttingDown = true;
    }
    
    private void OnDestroy()
    {
        m_ShuttingDown = true;
    }
}