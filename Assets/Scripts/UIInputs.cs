﻿using UnityEngine;
using UnityEngine.UI;

public class UIInputs : Singleton<UIInputs>
{
    protected UIInputs() { }

    [SerializeField] private Button automaticRotationButton;
    public Button AutomaticRotationButton { get { return automaticRotationButton; } }

    [SerializeField] private Toggle automaticRotationToggle_X, automaticRotationToggle_Y, automaticRotationToggle_Z;
    public Toggle AutomaticRotationToggle_X { get { return automaticRotationToggle_X; } }
    public Toggle AutomaticRotationToggle_Y { get { return automaticRotationToggle_Y; } }
    public Toggle AutomaticRotationToggle_Z { get { return automaticRotationToggle_Z; } }

    [SerializeField] private Toggle automaticRotationToggleSpaceLocal, automaticRotationToggleSpaceWorld;
    public Toggle AutomaticRotationToggleSpaceLocal { get { return automaticRotationToggleSpaceLocal; } }
    public Toggle AutomaticRotationToggleSpaceWorld { get { return automaticRotationToggleSpaceWorld; } }

    [SerializeField] private Slider automaticRotationSliderSpeed;
    public Slider AutomaticRotationSliderSpeed { get { return automaticRotationSliderSpeed; } }

    [SerializeField] private Button downloadProject;
    public Button DownloadProject { get { return downloadProject; } }
    [SerializeField] private Button change3DModel;
    public Button Change3DModel { get { return change3DModel; } }
}