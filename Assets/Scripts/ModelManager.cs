﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModelManager : MonoBehaviour
{
    [SerializeField] private GameObject[] models;
    private GameObject currentModel;
    private int ID = 0;
    private int count;

    private void Awake()
    {
        currentModel = models[0];
        count = models.Length;
        UIInputs.Instance.Change3DModel.onClick.AddListener(Change3DModel);
    }

    private void Start()
    {
        Change3DModel();
    }

    private void Change3DModel()
    {
        currentModel.SetActive(false);
        ID = (ID + 1) >= count ? 0 : ++ID;
        currentModel = models[ID];
        currentModel.SetActive(true);
    }
}