﻿using System;
using System.Collections;
using UnityEngine;
using TMPro;

public class AutoRotation : MonoBehaviour
{
    private float speedFactor = 10;
    private float spinSpeed = 5;
    public float SpinSpeed { get => spinSpeed * speedFactor; private set => spinSpeed = value; }

    private bool
        rotateX = false,
        rotateY = true,
        rotateZ = false;

    private bool rotating = true;
    private Space space = Space.World;

    [SerializeField] TextMeshProUGUI XValue, YValue, ZValue;

    private float lastXSpeed;

    private void Awake()
    {
        UIInputs.Instance.AutomaticRotationButton.onClick.AddListener(AutomationRotationButtonAction);
        UIInputs.Instance.AutomaticRotationToggle_X.onValueChanged.AddListener(SetActiveAutomaticRotationToggle_X);
        UIInputs.Instance.AutomaticRotationToggle_Y.onValueChanged.AddListener(SetActiveAutomaticRotationToggle_Y);
        UIInputs.Instance.AutomaticRotationToggle_Z.onValueChanged.AddListener(SetActiveAutomaticRotationToggle_Z);
        UIInputs.Instance.AutomaticRotationSliderSpeed.onValueChanged.AddListener(SetSpinSpeed);
        UIInputs.Instance.AutomaticRotationToggleSpaceWorld.onValueChanged.AddListener(SetRelativeSpaceToWorld);
        UIInputs.Instance.AutomaticRotationToggleSpaceLocal.onValueChanged.AddListener(SetRelativeSpaceToLocal);
        UIInputs.Instance.Change3DModel.onClick.AddListener(RestartRotation);

        if (rotating)
        {
            UIInputs.Instance.AutomaticRotationButton.image.color = Color.green;
            StartCoroutine(Rotating());
        }
        else
            UIInputs.Instance.AutomaticRotationButton.image.color = Color.red;
        XValue.text = (SpinSpeed * 1).ToString();
        YValue.text = (SpinSpeed * 2).ToString();
        ZValue.text = (SpinSpeed * 3).ToString();
    }

    #region Action

    private void AutomationRotationButtonAction()
    {
        if (!StartAutomation()) StopAutomation();
    }

    private bool StartAutomation()
    {
        if (rotating) return false;
        UIInputs.Instance.AutomaticRotationButton.image.color = Color.green;

        StartCoroutine(Rotating());
        return rotating = true;
    }

    private void StopAutomation()
    {
        rotating = false;
        UIInputs.Instance.AutomaticRotationButton.image.color = Color.red;
    }

    private IEnumerator Rotating()
    {
        yield return new WaitUntil(() => rotating);
        while (rotating)
        {
            var newXSpeed = SpinSpeed * 1;
            var newYSpeed = SpinSpeed * 2;
            var newZSpeed = SpinSpeed * 3;

            if (lastXSpeed != newXSpeed)
            {
                XValue.text = Math.Round((newXSpeed) - 0.5 / (Math.Pow(10, 1)), 1).ToString();
                YValue.text = Math.Round((newYSpeed) - 0.5 / (Math.Pow(10, 1)), 1).ToString();
                ZValue.text = Math.Round((newZSpeed) - 0.5 / (Math.Pow(10, 1)), 1).ToString();
            }

            transform.Rotate(
                rotateX ? newXSpeed * Time.deltaTime : 0,
                rotateY ? newYSpeed * Time.deltaTime : 0,
                rotateZ ? newZSpeed * Time.deltaTime : 0,
                space);

            lastXSpeed = newXSpeed;

            yield return null;
        }
    }

    #endregion

    private void RestartRotation()
    {
        transform.rotation = Quaternion.identity;
    }

    #region Set parameters

    private void SetActiveAutomaticRotationToggle_X(bool active)
    {
        rotateX = active;
    }
    private void SetActiveAutomaticRotationToggle_Y(bool active)
    {
        rotateY = active;
    }
    private void SetActiveAutomaticRotationToggle_Z(bool active)
    {
        rotateZ = active;
    }

    private void SetSpinSpeed(float value)
    {
        SpinSpeed = value;
    }

    private void SetRelativeSpaceToWorld(bool active)
    {
        space = active ? Space.World : Space.Self;
        if (active && UIInputs.Instance.AutomaticRotationToggleSpaceLocal.isOn) UIInputs.Instance.AutomaticRotationToggleSpaceLocal.isOn = false;
        else if (!UIInputs.Instance.AutomaticRotationToggleSpaceLocal.isOn) UIInputs.Instance.AutomaticRotationToggleSpaceLocal.isOn = true;
    }

    private void SetRelativeSpaceToLocal(bool active)
    {
        space = active ? Space.Self : Space.World;
        if (active && UIInputs.Instance.AutomaticRotationToggleSpaceWorld.isOn) UIInputs.Instance.AutomaticRotationToggleSpaceWorld.isOn = false;
        else if (!UIInputs.Instance.AutomaticRotationToggleSpaceWorld.isOn) UIInputs.Instance.AutomaticRotationToggleSpaceWorld.isOn = true;
    }

    #endregion
}